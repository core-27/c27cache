#!/usr/bin/env bash

poetry publish -n -v --build --username $PYPI_USERNAME --password $PYPI_PASSWORD 