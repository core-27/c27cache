import pytest
import os
import redis

REDIS_URL = os.environ.get("REDIS_URL", "redis://localhost:6379/3")
redis_client = redis.Redis.from_url(REDIS_URL)

class TestC27CacheKeys:
    @pytest.mark.asyncio
    async def test_generate_keys(self):
        redis_client.flushdb()
        from c27cache.config import C27Cache
        from c27cache.keys import C27CacheKeys
        namespace = 'test_namespace'
        C27Cache.init(redis_url=REDIS_URL, namespace=namespace)
        namespaced_key = await C27CacheKeys.generate_key(key='hello', config=C27Cache)
        assert namespaced_key == f'{namespace}:hello'
        
    @pytest.mark.asyncio
    async def test_generate_key_with_attr(self):
        redis_client.flushdb()
        from c27cache.config import C27Cache
        from c27cache.keys import C27CacheKeys

        class User:
            id: str = "112358"

        user = User()
        
        namespace = 'test_namespace'
        C27Cache.init(redis_url=REDIS_URL, namespace=namespace)
        namespaced_key = await C27CacheKeys.generate_key(key='hello.{}', config=C27Cache, obj=user, obj_attr='id')
        assert namespaced_key == f'{namespace}:hello.112358'
        
    @pytest.mark.asyncio
    async def test_generate_keys_with_attr(self):
        redis_client.flushdb()
        from c27cache.config import C27Cache
        from c27cache.keys import C27CacheKeys

        class User:
            id: str = "112358"

        user = User()
        
        namespace = 'test_namespace'
        C27Cache.init(redis_url=REDIS_URL, namespace=namespace)
        namespaced_keys = await C27CacheKeys.generate_keys(keys=['hello.{}', 'foo.{}'], config=C27Cache, obj=user, obj_attr='id')
        namespaced_keys = sorted(namespaced_keys)
        assert namespaced_keys[1] == f'{namespace}:hello.112358'
        assert namespaced_keys[0] == f'{namespace}:foo.112358'